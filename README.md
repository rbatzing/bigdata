# Big Data Analytics

A collection of guides and resources for studying various aspects of Big Data Analytics used in the Payap University Course CS424, Semester 1, 2021-2022 Academic year.

<img src="bigdatalogo.png" width="50%" align="right">

**Instructor:** Dr Bob Batzinger, Emeritus Instructor

    Payap University, Computer Science Department
    Chiang Mai 50000, Thailand
    LinkedIn: https://www.linkedin.com/in/robert-batzinger/
    WWW: rbatzing.github.io
    
**Guide Files:**

1. Installing GitLab
2. Installing R and RStudio
3. Installing TidyR
4. Starting a R Research Notebook
5. Stages in Data Analytics

**Research Notebooks:**

1. Brookline Bridge
2. COVID-19 Globally
3. COVID-19 Chiang Mai
4. Flight On Time Arrival
5. Bicycle Rental in NYC
6. NYC Weather 



